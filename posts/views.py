from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Post
# Create your views here.

def list(request):
    posts = Post.objects.all()
    return render(request, 'posts/list.html', {'posts':posts})

@login_required
def create(request):
    # Q2-2 : 게시물작성 GET
    # Q2-3 : 게시물작성 POST
    pass

@login_required
def like(request, id):
    # Q3-1 : 좋아요 기능 구현
    pass